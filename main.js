// setTimeout дозволяє викликати функцію один раз через певний проміжок часу.
//  setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.
// виконання func якнайшвидше. Але планувальник буде викликати функцію тільки після завершення виконання поточного коду
//  Для setInterval функція залишається в памяті до виклику clearInterval.
//  Функція посилається на зовнішнє середовище, Вони можуть зайняти набагато більше памяті, ніж сама функція. Тому, коли нам більше не потрібна запланована функція, краще її скасувати, навіть якщо вона дуже мала.



const allImg = Array.from(document.querySelectorAll("img"));
const playBtn = document.querySelector(".play");
const stopBtn = document.querySelector(".stop");

let showImg;
let nextIndex = 0;

const show_img = () => {
  for (let i = 0; i < allImg.length; i++) {
    if (i === nextIndex) {
      allImg[i].classList.add("show");
      allImg[i].classList.remove("hidden");
    } else {
      allImg[i].classList.remove("show");
      allImg[i].classList.add("hidden");
    }
  }

  ++nextIndex;

  if (nextIndex >= allImg.length) {
    nextIndex = 0;
  }
}

stopBtn.addEventListener("click", () => {
  clearInterval(showImg);

  stopBtn.disabled = true;
  playBtn.disabled = false;
});

playBtn.addEventListener("click", () => {
  showImg = setInterval(show_img, 3000);

  playBtn.disabled = true;
  stopBtn.disabled = false;
});

playBtn.click();